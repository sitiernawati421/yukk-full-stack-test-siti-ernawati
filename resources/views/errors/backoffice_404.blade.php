<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Laravel</title>

        {!! css(assetCss('backoffice/vendor/fontawesome-free/css/all.min.css')) !!}
    
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        {!! css(assetCss('backoffice/css/sb-admin-2.min.css')) !!}
    </head>

    <body id="page-top">
        <div id="wrapper" style="margin-top: 250px; background-color: #ffffff;">
            <div id="content-wrapper" style="background-color: #ffffff;">
                <div id="content">
                    <div class="container-fluid">
                        <div class="text-center">
                            <div class="error mx-auto" data-text="404">
                                404
                            </div>
                            <p class="lead text-gray-800 mb-5">
                                Page Not Found
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {!! js(assetJs('backoffice/vendor/jquery/jquery.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/bootstrap/js/bootstrap.bundle.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/jquery-easing/jquery.easing.min.js')) !!}
        {!! js(assetJs('backoffice/js/sb-admin-2.min.js')) !!}
    </body>
</html>