<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="">

        <title>Laravel</title>

        {!! css(assetCss('backoffice/vendor/fontawesome-free/css/all.min.css')) !!}
    
       <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        {!! css(assetCss('backoffice/css/sb-admin-2.min.css')) !!}
    </head>

    <body>
        <div class="container">
            <!-- outer row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-12">
                    <div class="">
                        <div class="card-body p-0">
                            <!-- card body -->
                            <div class="row">
                                <div class="col-lg-12 my-5">
                                    <h3>Hi, {{ $user['name'] }}</h3>
                                    <h3>Your Balance is {{ currency($user['balance'], 'IDR ') }}</h3>
                                    <div class="row mb-2">
                                        <div class="col-md-10">
                                            <a href="{{ route('add_transaction') }}" class="btn btn-primary">Add Transaction</a>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <a href="{{ route('do_logout') }}" class="btn btn-secondary">Logout</a>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-6">
                                            <form action="{{ route('dashboard') }}" method="get" autocomplete="off">
                                                <input type="search" name="search" class="form-control" value="{{ \Request::get('search') ? \Request::get('search') : '' }}" placeholder="enter for search">
                                            </form>
                                        </div>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Code</th>
                                            <th scope="col">Type</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Notes</th>
                                            <th scope="col">File</th>
                                            <th scope="col">Created At</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @if($transactions->count())
                                                @foreach($transactions as $key => $item)
                                                    <tr>
                                                        <th scope="row">{{ \Request::get('page') && \Request::get('page') > 1 ? (\Request::get('page') - 1) * 5 + ($key + 1) : $key + 1 }}</th>
                                                        <th>{{ $item['code'] }}</th>
                                                        <td>
                                                            @if($item['type'] == 1 )
                                                                <label class="badge badge-success">Topup</label>
                                                            @else
                                                                <label class="badge badge-danger">Transaction</label>
                                                            @endif
                                                        </td>
                                                        <th>{{ currency($item['amount'], 'IDR ') }}</th>
                                                        <th>{{ $item['notes'] }}</th>
                                                        <th>
                                                            @if($item['file'] != null)
                                                                <a href="{{ baseUrl() . '/public/upload/' . $item['file'] }}" target="_blank">{{ $item['file'] }}</a>
                                                            @else 
                                                                -
                                                            @endif
                                                        </th>
                                                        <th>{{ $item['created_at'] }}</th>
                                                    </tr>
                                                @endforeach
                                            @else 
                                                <tr>
                                                    <td colspan="7" class="text-center">
                                                        <label>Transaction is empty</label>
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>

                                    <?php echo $transactions->appends(['search' => \Request::get('search') ? \Request::get('search') : null])->render(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- bootstrap core javaScript-->
        {!! js(assetJs('backoffice/vendor/jquery/jquery.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/bootstrap/js/bootstrap.bundle.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/jquery-easing/jquery.easing.min.js')) !!}
        {!! js(assetJs('backoffice/js/sb-admin-2.min.js')) !!}
    </body>
</html>
