<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="">

        <title>Laravel</title>

        {!! css(assetCss('backoffice/vendor/fontawesome-free/css/all.min.css')) !!}
    
       <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        {!! css(assetCss('backoffice/css/sb-admin-2.min.css')) !!}
    </head>

    <body>
        <div class="container">
            <!-- outer row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-12">
                    <div class="">
                        <div class="card-body p-0">
                            <!-- card body -->
                            <div class="row">
                                <div class="col-lg-12 my-5">
                                    <h3>Add Transaction</h3>
                                    <h3>Your Balance is {{ currency($user['balance'], 'IDR ') }}</h3>
                                    <div class="row mb-2">
                                        <div class="col-md-12 mt-3">
                                          @if(session()->has('message'))
                                          <div class="alert alert-danger">
                                              {{ session()->get('message') }}
                                          </div>
                                          @endif
                                            <form action="{{ route('do_add_transaction') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                                                <div class="form-group">
                                                  <label>
                                                    Type<span class="text-danger">*</span>
                                                  </label>
                                                  <select class="form-control" name="type" required>
                                                    <option selected>-- Select Type --</option>
                                                    <option value="1">Topup</option>
                                                    <option value="2">Transaction</option>
                                                  </select>
                                                </div>
                                                <div class="form-group">
                                                  <label>
                                                    Amount<span class="text-danger">*</span>
                                                  </label>
                                                  <input type="number" name="amount" class="form-control" placeholder="Enter Amount" required>
                                                </div>
                                                <div class="form-group">
                                                  <label>
                                                    Notes<span class="text-danger">*</span>
                                                  </label>
                                                  <textarea class="form-control" name="notes" required>
                                                  </textarea>
                                                </div>
                                                <div class="form-group" id="file_upload">
                                                  <label>
                                                    Files<span class="text-danger">*</span>
                                                  </label><br>
                                                  <input type="file" name="file">
                                                </div>
                                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                                <a href="{{ route('dashboard') }}" class="btn btn-primary btn-secondary btn-block">Back</a>
                                              </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- bootstrap core javaScript-->
        {!! js(assetJs('backoffice/vendor/jquery/jquery.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/bootstrap/js/bootstrap.bundle.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/jquery-easing/jquery.easing.min.js')) !!}
        {!! js(assetJs('backoffice/js/sb-admin-2.min.js')) !!}
    </body>
</html>

<script>
    $(document).ready(function() {
        $('#file_upload').hide();

        $('[name="type"]').on('change', function() {
            if ($(this).val() == 1) {
                $('#file_upload').show();
                $('[name="file"]').attr('required', true);
            } else {
                $('#file_upload').hide();
                $('[name="file"]').attr('required', false);
            }
        });
    });
</script>