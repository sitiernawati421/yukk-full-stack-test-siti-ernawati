<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="">

        <title>Laravel</title>

        {!! css(assetCss('backoffice/vendor/fontawesome-free/css/all.min.css')) !!}
    
       <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        {!! css(assetCss('backoffice/css/sb-admin-2.min.css')) !!}
    </head>

    <body>
        <div class="container">
            <!-- outer row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-12">
                    <div class="">
                        <div class="card-body p-0">
                            <!-- card body -->
                            <div class="row">
                                <div class="col-lg-2">
                                </div>

                                <div class="col-lg-8 card o-hidden border-0 shadow-lg my-5">
                                    @if(session()->has('message'))
                                      <div class="alert alert-danger">
                                          {{ session()->get('message') }}
                                      </div>
                                    @endif
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">
                                                Register
                                            </h1>
                                        </div>
                                        
                                        <form action="{{ route('do_register') }}" method="post">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <div class="form-group">
                                            <label>
                                              Name<span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="name" class="form-control" aria-describedby="nameHelp" placeholder="Enter Name" required>
                                          </div>
                                          <div class="form-group">
                                            <label>
                                              Email<span class="text-danger">*</span>
                                            </label>
                                            <input type="email" name="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter Email" required>
                                          </div>
                                          <div class="form-group">
                                            <label>
                                              Password<span class="text-danger">*</span>
                                            </label>
                                            <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
                                          </div>
                                          <div class="form-group">
                                            <label>
                                              Confirm Password<span class="text-danger">*</span>
                                            </label>
                                            <input type="password" name="confirm_password" class="form-control" placeholder="Enter Confirm Password" required>
                                          </div>
                                          <button type="submit" class="btn btn-primary btn-block">Register</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- bootstrap core javaScript-->
        {!! js(assetJs('backoffice/vendor/jquery/jquery.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/bootstrap/js/bootstrap.bundle.min.js')) !!}
        {!! js(assetJs('backoffice/vendor/jquery-easing/jquery.easing.min.js')) !!}
        {!! js(assetJs('backoffice/js/sb-admin-2.min.js')) !!}
    </body>
</html>
