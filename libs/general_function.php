<?php 

function baseUrl()
{
    return URL::to('/');
}

function controller($name)
{
    $file = '\App\Http\Controllers\\$name';
    return $file;
}

function model($model)
{
    $file = "\App\Models\\$model";
    return new $file;
}

function assetCss($name){
    return baseUrl().'/assets/'.$name;
}

/* asset url */
function assetJs($name){
    return baseUrl().'/assets/'.$name;
}

function css($url)
{
    return '<link rel="stylesheet" href="'.$url.'" type="text/css">';
}

function js($url)
{
    return '<script src="'.$url.'"></script>';
}

function currency($num, $format = "Rp "){
	if($num === "") return "";
	$num = !is_numeric($num) ? 0 : $num;

    $currency = number_format($num, 0,',','.');
	return $format.$currency;
}

?>