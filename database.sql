-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Dec 23, 2023 at 06:21 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `yukk_full_stack_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `amount` double(14,2) DEFAULT NULL,
  `notes` varchar(100) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `user_id`, `type`, `amount`, `notes`, `file`, `created_at`) VALUES
(1, 'TRX-ONr6oJvboZ', 1, 1, 1000000.00, 'ini topup pertama kali', 'ahsan-avi-t9sr43kuJN0-unsplash.jpg', '2023-12-23 12:11:12'),
(2, 'TRX-WVziAQ0R1Q', 1, 1, 200000.00, 'ini topup kedua kali', 'alice-donovan-rouse-z9F_yK4Nmf8-unsplash.jpg', '2023-12-23 12:11:30'),
(3, 'TRX-886hJpoaD3', 1, 2, 300000.00, 'mau beli sayuran', NULL, '2023-12-23 12:11:40'),
(4, 'TRX-RrVbXpmp2p', 1, 1, 700000.00, 'tabungan                                                  ', '0D410486-09E8-44E5-8823-9DD0C9DFB2F7 2.JPG', '2023-12-23 12:13:30'),
(5, 'TRX-oGuBpsnWHp', 1, 1, 300000.00, 'nabung lagi', '—Pngtree—cute bts group cartoon illustration_7362350.png', '2023-12-23 12:13:45'),
(6, 'TRX-x7PU0lwPXU', 1, 2, 1200000.00, 'bayar kosan                                                   ', NULL, '2023-12-23 12:14:00'),
(8, 'TRX-vt3rjy5fex', 1, 1, 100000.00, 'nabung                                                  ', 'image (11).png', '2023-12-23 12:19:24'),
(9, 'TRX-TJI893bCya', 1, 1, 4000000.00, 'gajian                                                  ', '—Pngtree—cute bts group cartoon illustration_7362350.png', '2023-12-23 12:20:06'),
(10, 'TRX-gorDjlVXS3', 1, 1, 200000.00, 'lembur                                                  ', '—Pngtree—cute bts group cartoon illustration_7362350.png', '2023-12-23 12:20:23'),
(11, 'TRX-r4yD7tvCrd', 1, 2, 300000.00, 'bayar listrik                                                  ', NULL, '2023-12-23 12:20:32'),
(12, 'TRX-7Suoki8hlB', 1, 2, 300000.00, 'belanja shopee', NULL, '2023-12-23 12:20:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `balance` double(14,2) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `balance`, `created_at`) VALUES
(1, 'siti ernawati', 'sitiernawati421@gmail.com', 'Test123', 4600000.00, '2023-12-23 12:10:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;