<?php namespace App\Models;

class Users extends BaseEloquent
{
    protected static $elq = __CLASS__;

    public $table = "users";
    public $primaryKey = "id";

    /* timestamps */
	public $timestamps = false;
	const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';


    public function __construct()
    {
        parent::__construct();
    }
    

    protected $fillable = [
        "name",
        "email",
        "password",
        "balance",
        "created_at"
    ];

}

?>