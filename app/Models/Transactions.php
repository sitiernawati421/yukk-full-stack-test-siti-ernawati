<?php namespace App\Models;

class Transactions extends BaseEloquent
{
    protected static $elq = __CLASS__;

    public $table = "transactions";
    public $primaryKey = "id";

    /* timestamps */
	public $timestamps = false;
	const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';


    public function __construct()
    {
        parent::__construct();
    }
    

    protected $fillable = [
        "code",
        "user_id",
        "type",
        "amount",
        "notes",
        "file",
        "created_at"
    ];

}

?>