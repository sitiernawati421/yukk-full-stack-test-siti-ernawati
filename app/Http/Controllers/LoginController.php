<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class LoginController extends BaseController
{
    public function login()
    {
        if (\Session::has('userLoginToken')) {
            return redirect(route('dashboard'));
        }
        return view('login');
    }

    public function doLogin() {
        $email = \Request::input('email') ? \Request::input('email') : NULL;
        $password = \Request::input('password') ? \Request::input('password') : NULL;

        $user = Model('Users')->where([
            'email' => $email,
            'password' => $password
        ])->first();

        if (!$user) {
            return redirect()->back()->with('message', 'Email or Password is Wrong!');
        }

        \Session::put('userLoginToken', \Crypt::encrypt($user['id']));

        return redirect(route('dashboard'));
    }

    public function logout() {
        \Session::forget('userLoginToken');

        return redirect(route('login'));
    }
}