<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransactionController extends BaseController
{
    public function transaction()
    {
        if (!\Session::has('userLoginToken')) {
            return redirect(route('login'));
        }

        $id = \Crypt::decrypt(\Session::get('userLoginToken'));

        $user = Model('Users')->where([
            'id' => $id
        ])->first();

        return view('transaction', [
            'user' => $user
        ]);
    }

    public function doAddTransaction(Request $request) {
        if (!\Session::has('userLoginToken')) {
            return redirect(route('login'));
        }

        $user_id = \Request::input('user_id') ? \Request::input('user_id') : NULL;
        $type = \Request::input('type') ? \Request::input('type') : NULL;
        $amount = \Request::input('amount') ? \Request::input('amount') : NULL;
        $notes = \Request::input('notes') ? \Request::input('notes') : NULL;

        $fileName = null;
        if ($request->file()) {
            $file = $request->file('file');
            $request->file->move(public_path('upload'), $file->getClientOriginalName());
            $fileName =  $file->getClientOriginalName();
        }

        $user = model('Users')->where([
            'id' => $user_id
        ])->first();

        if (!$user) {
            if ($password != $confirm_password) {
                return redirect()->back()->with('message', 'User not found!');
            }
        }

        if ($type == 1) {
            $user->update([
                'balance' => $user['balance'] + $amount
            ]);
        } else {
            if ($amount > $user['balance']) {
                return redirect()->back()->with('message', 'Transaction Amount Cannot More Than Balance!');
            }

            $user->update([
                'balance' => $user['balance'] - $amount
            ]);
        }

        $data = model('Transactions')
            ->fill([
                'code' => "TRX-" . str_random(10),
                'user_id' => $user_id,
                'type' => $type,
                'amount' => $amount,
                'notes' => $notes,
                'file' => $fileName,
                'created_at' => Carbon::now()
            ])->save();
            
        return redirect(route('dashboard'));
    }
}