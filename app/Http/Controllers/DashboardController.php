<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class DashboardController extends BaseController
{
    public function dashboard()
    {
        if (!\Session::has('userLoginToken')) {
            return redirect(route('login'));
        }

        $id = \Crypt::decrypt(\Session::get('userLoginToken'));

        $user = model('Users')->where([
            'id' => $id
        ])->first();

        if (!$user) {
            \Session::forget('userLoginToken');
            return redirect(route('login'));
        }

        $search = \Request::get('search') ? \Request::get('search') : null;

        if ($search != null) {
            $transactions = model('Transactions')
                ->where(['user_id' => $id])
                ->where('code', 'like', '%' . $search . '%')
                ->orWhere('notes', 'like', '%' . $search . '%')
                ->orderBy('id', 'desc')
                ->paginate(5);
        } else {
            $transactions = model('Transactions')->where([
                'user_id' => $id
            ])->orderBy('id', 'desc')->paginate(5);
        }
    
        return view('dashboard', [
            'transactions' => $transactions,
            'user' => $user
        ]);
    }
}