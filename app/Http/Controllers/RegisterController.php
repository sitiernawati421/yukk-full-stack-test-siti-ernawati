<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;

class RegisterController extends BaseController
{
    public function register()
    {
        if (\Session::has('userLoginToken')) {
            return redirect(route('dashboard'));
        }
        return view('register');
    }

    public function doRegister() {
        $name = \Request::input('name') ? \Request::input('name') : NULL;
        $email = \Request::input('email') ? \Request::input('email') : NULL;
        $password = \Request::input('password') ? \Request::input('password') : NULL;
        $confirm_password = \Request::input('confirm_password') ? \Request::input('confirm_password') : NULL;
        
        if ($password != $confirm_password) {
            return redirect()->back()->with('message', 'Password and Confirm Password Must Be The Same!');
        }

        $user = model('Users')->where(['email' => $email])->first();
        if ($user) {
            return redirect()->back()->with('message', 'Email Already Registered!');
        }

        model('Users')->fill([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'balance' => 0,
            'created_at' => Carbon::now()
        ])->save();

        return redirect(route('login'));
    }
}