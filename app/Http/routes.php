<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Refresh Token */
Route::get('refresh-token', function(){
    return csrf_token();
});

Route::get('/', function () {
    return redirect(route('login'));
});

/* register */
Route::any('/register', ['uses' => 'RegisterController@register', 'as' => 'register']);
Route::post('/do-register', ['uses' => 'RegisterController@doRegister', 'as' => 'do_register']);

/* login */
Route::any('/login', ['uses' => 'LoginController@login', 'as' => 'login']);
Route::post('/do-login', ['uses' => 'LoginController@doLogin', 'as' => 'do_login']);

/* dashboard */
Route::any('/dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']);

/* Transaction */
Route::any('/transaction', ['uses' => 'TransactionController@transaction', 'as' => 'add_transaction']);
Route::post('/do-add-transaction', ['uses' => 'TransactionController@doAddTransaction', 'as' => 'do_add_transaction']);

/* logout */
Route::any('/logout', ['uses' => 'LoginController@logout', 'as' => 'do_logout']);