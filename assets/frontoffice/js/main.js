$(document).ready(function() {
    // Add item to card
    $(document).on('click', '#add_to_cart_button', function(event) {
        event.preventDefault();
        
        var itemId = $(this).attr('item-id');
        var price = Number($(this).attr('price'));
        var name = $(this).attr('name');
        var count = $(document).find('[name="quant[1]"]').val();
        $(document).find('#success-add-to-cart').show();
        shoppingCart.addItemToCart(itemId, price, parseInt(count), name);
        displayCart();
        $(document).find('#success-add-to-cart').fadeOut(3000);
    });

    var shoppingCart = (function() {
        cart = [];
        
        function Item(itemId, price, count, name) {
            this.itemId = itemId;
            this.price = price;
            this.count = count;
            this.name = name;
        }
    
        function saveCart() {
            sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
        }
        
        function loadCart() {
            cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
        }

        if (sessionStorage.getItem("shoppingCart") != null) {
            loadCart();
        }

        var obj = {};

        obj.addItemToCart = function(itemId, price, count, name) {
            for (var item in cart) {
                if (cart[item].itemId === itemId) {
                    cart[item].count = cart[item].count + count;
                    saveCart();
                    return;
                }
            }

            var item = new Item(itemId, price, count, name);
            cart.push(item);
            saveCart();
        }
        
        obj.totalCount = function() {
            var totalCount = 0;
            for (var item in cart) {
                totalCount += cart[item].count;
            }

            return totalCount;
        }

        obj.totalCart = function() {
            var totalCart = 0;
            for(var item in cart) {
                totalCart += cart[item].price * cart[item].count;
            }
            return Number(totalCart.toFixed(2));
        }

        obj.listCart = function() {
            var cartCopy = [];
            for(i in cart) {
                item = cart[i];
                itemCopy = {};
                for(p in item) {
                    itemCopy[p] = item[p];

                }
                itemCopy.total = Number(item.price * item.count).toFixed(2);
                cartCopy.push(itemCopy)
            }
            return cartCopy;
        }

        obj.removeItemFromCart = function(itemId) {
            for(var item in cart) {
              if(cart[item].itemId === itemId) {
                cart[item].count --;
                if(cart[item].count === 0) {
                  cart.splice(item, 1);
                }
                break;
              }
          }
          saveCart();
        }

        obj.addItemToCartCheckout = function(itemId) {
            for(var item in cart) {
                if(cart[item].itemId === itemId) {
                  cart[item].count ++;
                  if(cart[item].count === 0) {
                    cart.splice(item, 1);
                  }
                  break;
                }
            }
            saveCart();
        }

        obj.removeItemFromCartAll = function(itemId) {
            for(var item in cart) {
              if(cart[item].itemId === itemId) {
                cart.splice(item, 1);
                break;
              }
            }
            saveCart();
        }
			
		return obj;
	})();
    
    function displayCart() {
        var cartArray = shoppingCart.listCart();
        if (cartArray.length <= 0) $(document).find('.checkout-button').hide()
        var output = "";
        var checkoutList = "";
        var formCheckout = "";
        var totalAmount = 0;
        var customerType = $('[name="customer_type"]').val();

        for(var i in cartArray) {
            var packageId = cartArray[i].itemId;

            if (customerType == 'reseller' && cartArray[i].count >= 3) {
                if (cartArray[i].count >= 10) {
                    cartArray[i].price = $(document).find('#reseller-price-10-'+ packageId).val();
                } else if (cartArray[i].count >= 5) {
                    cartArray[i].price = $(document).find('#reseller-price-5-' + packageId).val();
                } else if (cartArray[i].count >= 3) {
                    cartArray[i].price = $(document).find('#reseller-price-3-' + packageId).val();
                }
            } else if (customerType == 'stocklist' && cartArray[i].count >= 50) {
                if (cartArray[i].count >= 250) {
                    cartArray[i].price = $(document).find('#stocklist-price-250-' + packageId).val();
                } else if (cartArray[i].count >= 100) {
                    cartArray[i].price = $(document).find('#stocklist-price-100-' + packageId).val();
                } else if (cartArray[i].count >= 50) {
                    cartArray[i].price = $(document).find('#stocklist-price-50-' + packageId).val();
                }
            } else {
                if ($(document).find('#add_to_cart_button').attr("item-id") == packageId) {
                    cartArray[i].price = $(document).find('#add_to_cart_button').attr("price");
                }
            }

            output += "<li>"
            + "<h4>"
            +   "<a href='javascript:;'>" + cartArray[i].name + "</a>"
            + "</h4>"
            + "<p class='quantity'>"
            + cartArray[i].count + "x - "
            + "<span class='amount'>Rp " + numWithDot(cartArray[i].price) + "</span>"
            + "</p>"
            + "</li>";

            checkoutList += '<tr>'
            + '<td class="product-des" data-title="Description">'
            +    '<p class="product-name"><a href="#">' + cartArray[i].name + '</a></p>'
            +    '<p class="product-des">Beli lebih banyak paket, dapatkan keuntungan yang melimpah.</p>'
            + '</td>'
            + '<td class="price" data-title="Price"><span>Rp ' + numWithDot(cartArray[i].price) + '</span></td>'
            + '<td class="qty" data-title="Qty">'
            +    '<div class="input-group">'
            +        '<div class="button minus">'
            +           '<button type="button" class="btn btn-primary btn-number minus-item" itemId="' + cartArray[i].itemId + '" data-type="minus" data-field="quant[1]">'
            +                '<i class="ti-minus"></i>'
            +           '</button>'
            +        '</div>'
            +        '<input type="text" name="quant[1]" itemId="' + cartArray[i].itemId + '" class="input-number"  data-min="1" data-max="300" value="' + cartArray[i].count + '" readonly>'
            +        '<div class="button plus">'
            +            '<button type="button" class="btn btn-primary btn-number plus-item" itemId="' + cartArray[i].itemId + '" data-type="plus" data-field="quant[1]">'
            +                '<i class="ti-plus"></i>'
            +            '</button>'
            +        '</div>'
            +    '</div>'
            + '</td>'
            + '<td class="total-amount" itemId="' + cartArray[i].itemId + '" data-title="Total"><span class="total">Rp ' + numWithDot(cartArray[i].count * cartArray[i].price) + '</span></td>'
            + '<td class="action" data-title="Remove"><a href="#" class="remove-item" itemId="' + cartArray[i].itemId + '"><i class="ti-trash remove-icon"></i></a></td>'
            + '</tr>';

            formCheckout += '<input type="hidden" class="form-control form-control-user" name="package_list_id[]" value="' + cartArray[i].itemId + '" required>'
            + '<input type="hidden" class="form-control form-control-user" name="package_price[]" value="' + numWithDot(cartArray[i].price) + '" required>'
            + '<input type="hidden" class="form-control form-control-user" name="package_total[]" value="' + cartArray[i].count + '" required>'
            + '<input type="hidden" class="form-control form-control-user" name="total_price[]" value="' + numWithDot(cartArray[i].count * cartArray[i].price) + '" required>'

            totalAmount += (cartArray[i].count * cartArray[i].price);
        }

        $('.shopping-list').html(output);
        $('.table-row-checkout-list').html(checkoutList);
        $('.form-checkout').html(formCheckout);
        $('.total-amount-checkout').html("Rp " + numWithDot(totalAmount));
        $('.total-count').html(shoppingCart.totalCount());
    }

    function numWithDot(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $('.table-row-checkout-list').on("click", ".minus-item", function(event) {
        var itemId = $(this).attr('itemId');
        shoppingCart.removeItemFromCart(itemId);
        displayCart();
    });

    $('.table-row-checkout-list').on("click", ".plus-item", function(event) {
        var itemId = $(this).attr('itemId');
        shoppingCart.addItemToCartCheckout(itemId);
        displayCart();
    });

    $('.table-row-checkout-list').on("click", ".remove-item", function(event) {
        var itemId = $(this).attr('itemId');
        shoppingCart.removeItemFromCartAll(itemId);
        displayCart();
    });

    $(document).on("click", "[clear-cart]", function() {
        sessionStorage.clear();
    });

    displayCart();

    // $(document).on('keyup keypress', '[name="quant[1]"]', function () {
    //     var itemId = $(this).attr('itemId');
    //     var totalAmountItemId = $('.table-row-checkout-list').find('.total-amount').attr('itemId');
    //     if (itemId == totalAmountItemId) {
    //         totalAmountItemId = 1
    //     }
    // });
    
});